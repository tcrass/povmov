#!/usr/bin/perl

use strict;
use warnings;

#=== Global vars ======================================================

my $VERSION = "1.0.1";

my $MOVIE_DESCRIPTION_FILE = undef;

my $POV_COMMAND = "povray +A +FN -HI%c -I%i -O%o -W%w -H%h";
my $COPY_COMMAND = "cp -f %s %t";
if ($^O =~ m/Win/) {
  $POV_COMMAND = "pvengine /EXIT +A +FN -HI%c -I%i -O%o -W%w -H%h";
  $COPY_COMMAND = "copy /Y %s %t";
}
my $OUT_PREFIX = "povmov";
my $OUT_EXTENSION = "png";
my $FPS = 12;
my $LOOPED = 0;
my $WIDTH = 320;
my $HEIGHT = 200;
my $FROM_SCENE = 0;
my $FROM_SHOT = 0;
my $TO_SCENE = undef;
my $TO_SHOT = undef;
my $VERBOSE = 0;

my $ERR_ARGUMENT = 1;
my $ERR_MOVIE_DESCRIPTION_FILE_OPEN = 2;
my $ERR_MOVIE_DESCRIPTION_FILE_GENERAL_FORMAT = 3;
my $ERR_MOVIE_DESCRIPTION_FILE_SCENE_FORMAT = 4;
my $ERR_MOVIE_DESCRIPTION_FILE_SHOT_WITHOUT_SCENE = 5;
my $ERR_MOVIE_DESCRIPTION_FILE_NO_SCENE = 6;
my $ERR_MOVIE_DESCRIPTION_FILE_NO_SHOT = 7;
my $ERR_MOVIE_DESCRIPTION_FILE_SHOT_DURATION_ZERO = 8;
my $ERR_ILLEGAL_START_END_FORMAT = 9;
my $ERR_POVRAY_INCLUDE_FILE_OPEN = 10;

my @SCENES = ();
my $MOVIE = undef;

my $INCLUDE_FILE = "povmov.inc";


#=== Main program =====================================================

&readSettings();
&renderFrames();


#=== Subroutines ======================================================


#--- Subroutines for parsing command line options and the movie
#    description files as well as setting up the movie data structure

sub readSettings {
  if (scalar(@ARGV) == 0) {
    &printUsageAndExit();
  }
  $MOVIE_DESCRIPTION_FILE = pop(@ARGV);
  &parseMovieDescriptionFile($MOVIE_DESCRIPTION_FILE, 1);
  while (scalar(@ARGV) > 0) {
    my $arg = shift(@ARGV);
    if (($arg eq "-v") || ($arg eq "--verbose")) {
      $VERBOSE = 1;
    }
    elsif (($arg eq "-l") || ($arg eq "--looped")) {
      $LOOPED = 1;
    }
    else {
      if (scalar(@ARGV) > 0) {
        if (($arg eq "-w") || ($arg eq "--width")) {
          $WIDTH = shift(@ARGV);
          next;
        }
        elsif (($arg eq "-h") || ($arg eq "--height")) {
          $HEIGHT = shift(@ARGV);
          next;
        }
        elsif (($arg eq "-r") || ($arg eq "--fps")) {
          $FPS = shift(@ARGV);
          next;
        }
        elsif (($arg eq "-o") || ($arg eq "--out-prefix")) {
          $OUT_PREFIX = shift(@ARGV);
          next;
        }
        elsif (($arg eq "-e") || ($arg eq "--out-extension")) {
          $OUT_EXTENSION = shift(@ARGV);
          next;
        }
        elsif (($arg eq "-f") || ($arg eq "--from")) {
          ($FROM_SCENE, $FROM_SHOT) = &parseFromToSpecification(shift(@ARGV));
          next;
        }
        elsif (($arg eq "-t") || ($arg eq "--to")) {
          ($TO_SCENE, $TO_SHOT) = &parseFromToSpecification(shift(@ARGV));
          next;
        }
        else {
          print STDERR "Unknown command line argument: $arg\n";
          exit($ERR_ARGUMENT);
        }
      }
    }
  }
  &parseMovieDescriptionFile($MOVIE_DESCRIPTION_FILE, 2);
}

sub printUsageAndExit {
  print <<EOS;

povmov.pl -- povray movie rendering helper script (version $VERSION)
===============================================================================
Usage: povmov.pl [options] movie_description_file
Settings specified in the movie_description_file (see "povmov.mdf" for syntax
examples) can be overridden by the following options:
--verbose|-v:             Be verbose, tell what you are currently doing
--looped|-l:              Consider movie as looped
--width|-w <x>:           Render frames with a width of <x> pixels [320]
--height|-h <y>:          Render frames with a height of <y> pixels [200]
--fps|-r <n>:             Movie has a frame rate of <n> frames per second [12]
--out-prefix|-o <pre>:    Prepend <pre> to the frames' filenames [povmov]
--out-extension|-e <ext>: Append .<ext> to the frames' filenames [png]
--from|-f <init>:         Start rendering at scene/shot specified by <init> [*]
--to|-t <final>:          Rendering until scene/shot specified by <final> [*]
Initial/final frame specification format: (*|<n>[:(*|<m>)])
Examples: -f * -t * renders everything
          -f * -t 8 renders from start to scene 8 (including)
          -f * -t 8:* (same as above)
          -f 8 -t 8 renders scene 8 only
          -f 7:2 to 8 renders from scene 7, shot 2, to scene 8 (including)
          -f 7:2 to * renders from scene 7, shot 2, to end of movie
Run "povmov.pl povmov.mdf" for a sample render and see "povmov.pov" for how
to use povmov.pl in your scene files.
EOS
  exit(0);
}

sub parseMovieDescriptionFile {

  sub throwParseError {
    my $line = shift;
    my $msg = shift;
    my $exit_code = shift;
    print STDERR "Error in movie description file in line ", $line + 1, ":\n";
    print STDERR "$msg\n";
    exit($exit_code);
  }

  $MOVIE_DESCRIPTION_FILE = shift;
  my $pass = shift;
  if (open(MDF, $MOVIE_DESCRIPTION_FILE)) {
    my @lines = <MDF>;
    close(MDF);
    chomp(@lines);

    my $inScenePart = 0;
    my $scene = undef;
    for (my $i = 0; $i < scalar(@lines); $i++) {
      my $line = $lines[$i];
      $line =~ s/^\s*//;
      unless ($line =~ m/^#/) {
        $line =~ s/[^\^]#.*$//;
        if ($line =~ m/^\s*(\S+)\s*=\s*(.+)$/) {
          my $key = $1;
          my $val = $2;
          $key = lc($key);
          $val =~ s/\s*$//;
          if ($key eq "scene") {
            $inScenePart = 1;
          }
          if (($pass == 1) && (not $inScenePart) ) {
            if ($key eq "pov_command") {
              $POV_COMMAND = $val;
            }
            elsif ($key eq "copy_command") {
              $COPY_COMMAND = $val;
            }
            elsif ($key eq "fps") {
              $FPS = $val;
            }
            elsif ($key eq "out_prefix") {
              $OUT_PREFIX = $val;
            }
            elsif ($key eq "out_extension") {
              $OUT_EXTENSION = $val;
            }
            elsif ($key eq "looped") {
              $LOOPED = $val;
            }
            elsif ($key eq "verbose") {
              $VERBOSE = $val;
            }
            elsif ($key eq "width") {
              $WIDTH = $val;
            }
            elsif ($key eq "height") {
              $HEIGHT = $val;
            }
            elsif ($key eq "from") {
              ($FROM_SCENE, $FROM_SHOT) = &parseFromToSpecification($val);
            }
            elsif ($key eq "to") {
              ($TO_SCENE, $TO_SHOT) = &parseFromToSpecification($val);
            }
            else {
              &throwParseError($i, "Unreckognised token $key in general part!",
                               $ERR_MOVIE_DESCRIPTION_FILE_GENERAL_FORMAT);
            }
          }
          if (($pass == 2) && $inScenePart) {
            if ($key eq "scene") {
              if (!defined($MOVIE)) {
                $MOVIE = new Movie($FPS, $LOOPED);
              }
              if (defined($scene)) {
                if (scalar($scene->getShots() == 0)) {
                  &throwParseError($i, "Found scene without shots!",
                                   $ERR_MOVIE_DESCRIPTION_FILE_NO_SHOT);
                }
                $MOVIE->addScene($scene);
              }
              $scene = new Scene($val);
            }
            elsif ($key eq "shot") {
              if (not defined($scene)) {
                &throwParseError($i, "Attempt to declare a shot without previous scene declaration!",
                                 $ERR_MOVIE_DESCRIPTION_FILE_SHOT_WITHOUT_SCENE);
              }
              else {
                if ($val == 0) {
                  &throwParseError($i, "Found shot with zero duration!",
                                   $ERR_MOVIE_DESCRIPTION_FILE_SHOT_DURATION_ZERO);
                }
                my $shot = new Shot($val);
                $scene->addShot($shot);
              }
            }
            else {
              &throwParseError($i, "Unreckognised token $key in scene part!",
                               $ERR_MOVIE_DESCRIPTION_FILE_SCENE_FORMAT);
            }
          }
        }
      }
    }
    if (defined($scene)) {
      if (scalar($scene->getShots()) == 0) {
        &throwParseError(scalar(@lines), "Found scene without shots!",
                         $ERR_MOVIE_DESCRIPTION_FILE_NO_SHOT);
      }
      $MOVIE->addScene($scene);
    }
    if (($pass == 2) && (scalar($MOVIE->getScenes()) == 0)) {
      &throwParseError(scalar(@lines), "No scene definition found!",
                       $ERR_MOVIE_DESCRIPTION_FILE_NO_SCENE);
    }
  }
  else {
    print STDERR "Error opening movie description file $MOVIE_DESCRIPTION_FILE!\n";
    exit($ERR_MOVIE_DESCRIPTION_FILE_OPEN);
  }
}

sub parseFromToSpecification {
  my $val = shift;
  my $scene = undef;
  my $shot = undef;
  my @spec = split(/:/, $val);
  if (defined($spec[0]) && ($spec[0] ne "*")) {
    $scene = shift(@spec);
    if (defined($spec[0]) && ($spec[0] ne "*")) {
      $shot = shift(@spec);
    }
  }
  return ($scene, $shot);
}


#--- Subroutines for rendering the individual frames

sub renderFrames {
  my $movieDuration = $MOVIE->getDuration();
  my $movieFrames = $movieDuration * $FPS;
  if (not $LOOPED) {
    $movieFrames++;
  }
  my $staticShotFile = undef;
  my $lastScene = -1;
  my $lastShot = -1;

  $MOVIE->reset();
  while ($MOVIE->hasMoreFrames()) {
    my $movieTime = $MOVIE->getCurrentTime();
    my $relMovieTime = $MOVIE->getCurrentTime()/$MOVIE->getDuration();
    my $movieFrame = $MOVIE->getCurrentFrame();
    my $scenesInMovie = scalar($MOVIE->getScenes());

    my $sceneIndex = $MOVIE->getCurrentSceneIndex();
    my $sceneTime = $MOVIE->getSceneTime();
    my $sceneFrame = $MOVIE->getSceneFrame();
    my $scene = $MOVIE->getCurrentScene();
    my $sceneFile = $scene->getSceneFile();
    my $sceneDuration = $scene->getDuration();
    my $sceneFrames = $sceneDuration * $FPS;
    my $relSceneTime = $sceneTime/$sceneDuration;
    my $shotsInScene = scalar($scene->getShots());

    my $shotIndex = $MOVIE->getCurrentShotIndex();
    my $shotTime = $MOVIE->getShotTime();
    my $shotFrame = $MOVIE->getShotFrame();
    my $shot = $MOVIE->getCurrentShot();
    my $shotDuration = $shot->getDuration();
    my $shotFrames = $shotDuration * $FPS;
    my $relShotTime = $shotTime/$shotDuration;

    if (
         (
           (not defined($FROM_SCENE)) ||
           (
             ($sceneIndex >= $FROM_SCENE) &&
             ((not defined($FROM_SHOT)) || ($shotIndex >= $FROM_SHOT))
           )
          ) &&
          (
            (not defined($TO_SCENE)) ||
            (
              ($sceneIndex <= $TO_SCENE) &&
              ((not defined($TO_SHOT)) || ($shotIndex <= $TO_SHOT))
            )
          )
       )
    {
      my $t0 = time;
      if ($VERBOSE) {
        print "Rendering scene $sceneIndex, shot $shotIndex, frame #$shotFrame (of $shotFrames)... ";
      }

      my $inc = "// POV-Ray include file\n// created by povmov while rendering movie $MOVIE_DESCRIPTION_FILE\n\n";
      $inc = $inc . "#declare movieDescriptionFile = \"$MOVIE_DESCRIPTION_FILE\";\n";
      $inc = $inc . "#declare movieTime = $movieTime;\n";
      $inc = $inc . "#declare movieFrame = $movieFrame;\n";
      $inc = $inc . "#declare relMovieTime = $relMovieTime;\n";
      $inc = $inc . "#declare movieDuration = $movieDuration;\n";
      $inc = $inc . "#declare movieFrames = $movieFrames;\n";
      $inc = $inc . "#declare scenesInMovie = $scenesInMovie;\n\n";

      $inc = $inc . "#declare scene = $sceneIndex;\n";
      $inc = $inc . "#declare sceneFile = \"" . $scene->getSceneFile() . "\";\n";
      $inc = $inc . "#declare sceneTime = $sceneTime;\n";
      $inc = $inc . "#declare sceneFrame = $sceneFrame;\n";
      $inc = $inc . "#declare relSceneTime = $relSceneTime;\n";
      $inc = $inc . "#declare sceneDuration = $sceneDuration;\n";
      $inc = $inc . "#declare sceneFrames = $sceneFrames;\n";
      $inc = $inc . "#declare shotsInScene = $shotsInScene;\n\n";

      $inc = $inc . "#declare shot = $shotIndex;\n";
      $inc = $inc . "#declare shotTime = $shotTime;\n";
      $inc = $inc . "#declare shotFrame = $shotFrame;\n";
      $inc = $inc . "#declare relShotTime = $relShotTime;\n";
      $inc = $inc . "#declare shotDuration = $shotDuration;\n";
      $inc = $inc . "#declare shotFrames = $shotFrames;\n\n";

      my $outFile = sprintf("$OUT_PREFIX%.3d%.3d%.6d.$OUT_EXTENSION", $sceneIndex, $shotIndex, $shotFrame);

      if (($sceneIndex != $lastScene) || ($shotIndex != $lastShot)) {
        &deleteShotFrames($sceneIndex, $shotIndex);
        $staticShotFile = undef;
      }

      if ((not $shot->isStatic()) || (not defined($staticShotFile))) {
        if (open(INC, ">$INCLUDE_FILE")) {
          print INC $inc;
          close(INC);
          my $cmd = $POV_COMMAND;
          $cmd =~ s/%c/$INCLUDE_FILE/g;
          $cmd =~ s/%i/$sceneFile/g;
          $cmd =~ s/%w/$WIDTH/g;
          $cmd =~ s/%h/$HEIGHT/g;
          $cmd =~ s/%o/$outFile/g;

          system($cmd);

          if ($shot->isStatic()) {
            $staticShotFile = $outFile;
          }
          else {
            $staticShotFile = undef;
          }
        }
        else {
          print STDERR "Error while opening include file for writing!\n";
          exit($ERR_POVRAY_INCLUDE_FILE_OPEN);
        }
      }
      else {
        my $cmd = $COPY_COMMAND;
        $cmd =~ s/%s/$staticShotFile/g;
        $cmd =~ s/%t/$outFile/g;
        system($cmd);
      }

      my $t1 = time;
      if ($VERBOSE) {
        my $dt = $t1 - $t0;
        my $min = int($dt/60);
        my $sec = sprintf("%.2d", $dt - 60*$min);
        print "($min min $sec sec)\n";
      }

    }

    $lastScene = $sceneIndex;
    $lastShot = $shotIndex;

    $MOVIE->goToNextFrame();

  }
}

sub deleteShotFrames {
  my $scene = shift;
  my $shot = shift;
  my $files = sprintf("$OUT_PREFIX%.3d%.3d*$OUT_EXTENSION", $scene, $shot);
  my @files = glob($files);
  foreach my $file (@files) {
    unlink($file);
  }
}


#=== Movie class ======================================================

package Movie;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {};
  $self->{FPS} = shift;
  $self->{LOOPED} = shift;
  $self->{SCENES} = [];
  $self->{DURATION} = 0;
  bless ($self, $class);
  $self->reset();
  return $self;
}

sub getFps {
  my $self = shift;
  return $self->{FPS};
}

sub isLooped {
  my $self = shift;
  return $self->{LOOPED};
}

sub getDuration {
  my $self = shift;
  return $self->{DURATION};
}

sub addScene {
  my $self = shift;
  my $scene = shift;
  $scene->setStartTime($self->getDuration());
  $self->{DURATION} += $scene->getDuration();
  push(@{$self->{SCENES}}, $scene);
}

sub getScenes {
  my $self = shift;
  return @{$self->{SCENES}};
}

sub getCurrentSceneIndex {
  my $self = shift;
  return $self->{CURRENT_SCENE_INDEX};
}

sub getCurrentScene {
  my $self = shift;
  my @scenes = $self->getScenes();
  return $scenes[$self->getCurrentSceneIndex()];
}

sub getCurrentShotIndex {
  my $self = shift;
  return $self->{CURRENT_SHOT_INDEX};
}

sub getCurrentShot {
  my $self = shift;
  my $scene = $self->getCurrentScene();
  my @shots = $scene->getShots();
  return $shots[$self->getCurrentShotIndex()];
}

sub getSceneTime {
  my $self = shift;
  return $self->{SCENE_TIME}
}

sub getShotTime {
  my $self = shift;
  return $self->{SHOT_TIME}
}

sub getSceneFrame {
  my $self = shift;
  return $self->{SCENE_FRAME}
}

sub getShotFrame {
  my $self = shift;
  return $self->{SHOT_FRAME}
}

sub getCurrentTime {
  my $self = shift;
  return $self->{CURRENT_TIME};
}

sub getCurrentFrame {
  my $self = shift;
  return $self->{CURRENT_FRAME};
}

sub reset {
  my $self = shift;
  $self->{CURRENT_TIME} = 0;
  $self->{CURRENT_FRAME} = 0;
  if (scalar($self->getScenes()) > 0) {
    $self->{CURRENT_SCENE_INDEX} = 0;
  }
  else {
    $self->{CURRENT_SCENE_INDEX} = -1;
  }
  $self->{SCENE_TIME} = 0;
  $self->{SCENE_FRAME} = 0;
  if ($self->{CURRENT_SCENE_INDEX} < 0) {
    $self->{CURRENT_SHOT_INDEX} = -1;
  }
  else {
    $self->{CURRENT_SHOT_INDEX} = 0;
  }
  $self->{SHOT_TIME} = 0;
  $self->{SHOT_FRAME} = 0;
  $self->{DELTA_T} = 1/$self->getFps();
  $self->{FINAL_TIME} = $self->getDuration();
  if (not $self->isLooped()) {
    $self->{FINAL_TIME} += $self->{DELTA_T};
  }
}

sub hasMoreFrames {
  my $self = shift;
  if ($self->{CURRENT_SCENE_INDEX} < 0) {
    $self->reset();
  }
  if ($self->{CURRENT_TIME} < $self->{FINAL_TIME}) {
    return 1;
  }
  else {
    return 0;
  }
}

sub goToNextFrame {
  my $self = shift;
  $self->{CURRENT_TIME} += $self->{DELTA_T};
  $self->{SCENE_TIME} += $self->{DELTA_T};
  $self->{SHOT_TIME} += $self->{DELTA_T};
  $self->{CURRENT_FRAME}++;
  $self->{SCENE_FRAME}++;
  $self->{SHOT_FRAME}++;

  my @scenes = $self->getScenes();
  my $scene = $scenes[$self->getCurrentSceneIndex()];
  my @shots = $scene->getShots();
  my $shot = $shots[$self->getCurrentShotIndex()];
  if ($self->getCurrentTime() < $self->getDuration()) {
    while ($self->getCurrentTime() >= ($shot->getStartTime() + $shot->getDuration())) {
      $self->{CURRENT_SHOT_INDEX}++;
      $self->{SHOT_FRAME} = 0;
      if ($self->getCurrentShotIndex() >= scalar($scene->getShots())) {
        $self->{CURRENT_SCENE_INDEX}++;
        $self->{CURRENT_SHOT_INDEX} = 0;
        $self->{SCENE_FRAME} = 0;
      }
      $scene = $scenes[$self->getCurrentSceneIndex()];
      @shots = $scene->getShots();
      $shot = $shots[$self->getCurrentShotIndex()];
      $self->{SCENE_TIME} = $self->getCurrentTime() - $scene->getStartTime();
      $self->{SHOT_TIME} = $self->getCurrentTime() - $shot->getStartTime();
    }
  }
}


#=== Scene class ======================================================

package Scene;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {};
  $self->{SCENE_FILE} = shift;
  $self->{SHOTS} = [];
  $self->{DURATION} = 0;
  $self->{START_TIME} = 0;
  bless ($self, $class);
  return $self;
}

sub getSceneFile {
  my $self = shift;
  return $self->{SCENE_FILE};
}

sub getDuration {
  my $self = shift;
  return $self->{DURATION};
}

sub getStartTime {
  my $self = shift;
  return $self->{START_TIME};
}

sub addShot() {
  my $self = shift;
  my $shot = shift;
  push(@{$self->{SHOTS}}, $shot);
  $self->{DURATION} += $shot->getDuration();
}

sub getShots() {
  my $self = shift;
  return @{$self->{SHOTS}};
}

sub setStartTime {
  my $self = shift;
  $self->{START_TIME} = shift;
  my $t = $self->getStartTime();
  foreach my $shot($self->getShots()) {
    $shot->setStartTime($t);
    $t += $shot->getDuration();
  }
}


#=== Shot class =======================================================

package Shot;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {};
  $self->{DURATION} = shift;
  $self->{START_TIME} = 0;
  bless ($self, $class);
  if ($self->getDuration() < 0) {
    $self->{DURATION} = -$self->getDuration();
    $self->{STATIC} = 1;
  }
  else {
    $self->{STATIC} = 0;
  }
  return $self;
}

sub getDuration {
  my $self = shift;
  return $self->{DURATION};
}

sub isStatic {
  my $self = shift;
  return $self->{STATIC};
}

sub getStartTime {
  my $self = shift;
  return $self->{START_TIME};
}

sub setStartTime {
  my $self = shift;
  $self->{START_TIME} = shift;
}
