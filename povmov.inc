// POV-Ray include file
// created by povmov while rendering movie povmov.mdf

#declare movieDescriptionFile = "povmov.mdf";
#declare movieTime = 4;
#declare movieFrame = 48;
#declare relMovieTime = 0.8;
#declare movieDuration = 5;
#declare movieFrames = 61;
#declare scenesInMovie = 1;

#declare scene = 0;
#declare sceneFile = "povmov.pov";
#declare sceneTime = 4;
#declare sceneFrame = 48;
#declare relSceneTime = 0.8;
#declare sceneDuration = 5;
#declare sceneFrames = 60;
#declare shotsInScene = 4;

#declare shot = 3;
#declare shotTime = 2.66453525910038e-015;
#declare shotFrame = 0;
#declare relShotTime = 2.66453525910038e-015;
#declare shotDuration = 1;
#declare shotFrames = 12;

