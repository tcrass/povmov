//=====================================================================
// povmov.pl example POV-Ray file
//
// The resulting movie will show a ball being bounced off by a brick 
// wall.
//
//=====================================================================

// When called by povmov.pl, an file specifying values for the
// following variables will automatically be included:
/*

movieDescriptionFile - name of movie description file (e.g. "povmov.mdf")
movieTime            - current time (in seconds) in movie
movieFrame           - current frame number in movie
relMovieTime         - relative time in movie [0..1]
movieDuration        - duration of whole movie (in seconds)
movieFrames          - total number of frames in movie
scenesInMovie        - number of scenes in movie

scene                - number of current scene (starting from 0)
sceneFile            - file associated with current scene (i.e. *this* file)
sceneTime            - current time (in seconds) relative to scene start
sceneFrame           - current frame number relative to scene start
relSceneTime         - relative time relative to scene start [0..1]
sceneDuration        - duration of current scene (in seconds)
sceneFrames          - total number of frames in current scene
shotsInScene         - number of shots in current scene

shot                 - number of current shot (starting from 0)
shotTime             - current time (in seconds) relative to shot start
shotFrame            - current frame number relative to shot start
relShotTime          - relative time relative to shot start [0..1]
shotDuration         - duration of current shot (in seconds)
shotFrames           - total number of frames in current shot
*/


// If POV-Ray has not been called by povmov.pl (e.g. during scene
// development and debugging), the above menioned variables will not 
// be defined. In this case, we may want to explicitly set some of them
#ifndef (movieDescriptionFile)
  #declare shot = 3;
  #declare relShotTime = 0;
#end

//--- Basic scene setup -----------------------------------------------

//--- Camera
camera {
  location <-8, 6, -15>
  look_at <2, 4, 0>
}

//--- Light source
light_source { <-500, 500, -100>
  color rgb 1
}            


//--- Blue sky
sky_sphere {
  pigment {
    gradient y
    color_map {
      [0.4 color rgb <1, 1, 1>]
      [0.6 color rgb <0.5, 0.5, 1>]
    }
    scale 2
    translate -y
  }
}

//--- Checker plane
plane {y 0
  texture {
    pigment {
      checker
      color rgb 1
      color rgb 0
    }
    scale 2
  }
  finish {
    diffuse 0.7
    brilliance 0.1
    reflection 0.1
  }
}

//--- A piece of brick wall
box {<5, 0, -5> <6, 8, 5>
  texture {
    pigment {
      brick
      scale 0.5
    }
    finish {
      phong 1
      phong_size 20
      brilliance 1
      diffuse 0.7
      ambient 0.4
    }
    // Ah! When rendering shot 1, we want the brick wall's
    // surface to be distroted by a moving wave pattern
    #if (shot = 1)
      normal {
        // During the shot (relShotTime changing from 0 to 1),
        // we want the waves to become shallower
        waves 1 - relShotTime
        // Two "wave packages" are to travel outward during
        // the shot
        phase -2*relShotTime
        scale 0.25
        rotate z*90
        translate 2*y
      }
    #end
  }
}

// As mentioned, the movie will show a ball being bounced off by the 
// brick wall. For moving the ball along, we have to track its
// position and rotation. For each frame, current translation and 
// rotation values are calculated and stored in ball_pos and ball_rot,
// respectively.

// The movie has just one scene, so no further examination of the
// scene variable's value is required.
// The scene has four shots in which different things happen to the
// ball.

#switch (shot)
  // Shot 0: The ball rolls over the ground towards the wall
  #case (0)
    #declare ball_pos = <3, 2, 0> - 18*(1 - relShotTime)*x;
    #declare ball_rot = 360 * (5 - ball_pos.x) / (4 * 3.1415926);
  #break
  // Shot 1: The ball is bounced off the wall and moves along a 
  // parabolic curve
  #case (1)
    #declare ball_pos = <3, 2, 0> - 5*relShotTime*x + 10 * (0.25 - ((relShotTime - 0.5)*(relShotTime - 0.5)));
    #declare ball_rot = -360 * relShotTime;
  #break
  // Shot 2: The ball has reached the ground again and rolls out
  #case (2)
    #declare ball_pos = <-2, 2, 0> - (4 - 4*(1 - relShotTime)*(1 - relShotTime))*x;
    #declare ball_rot = 360 * (-2 - ball_pos.x) / (4 * 3.1415926);
  #break
  // Shot 3 is static -- the ball just stays where it stopped rolling
  #case (3)
    #declare ball_pos = <-6, 2, 0>;
    #declare ball_rot = 360/3.1415926;
  #break
#end


// Now, since we know the ball's position and rotational state, we
// can finally place it into the scene
sphere {0 2
  texture {
    pigment {
      gradient x
      color_map {
        [0.5 color rgb <0.1, 0.8, 0.1>]
        [0.5 color rgb <1.0, 0.5, 0>]
      }
      scale 0.5
      rotate 45*y
    }
    finish {
      phong 0.8
      phong_size 5
      brilliance 1
      diffuse 0.9  
      ambient 0.4
    }
  }
  rotate ball_rot*z
  translate ball_pos
} 
